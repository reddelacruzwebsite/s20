"use strict";

// S 20 - Javascript Repetition Control Structures

// ACTIVITY


/*
PART 1: 

	- Create a variable number that will store the value of the number provided by the user via the prompt.

	- Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

	- Create a condition that if the current value is less than or equal to 50, stop the loop.

	- Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

	- Create another condition that if the current value is divisible by 5, print the number.

*/

	// CODE HERE:
	
const number = prompt("Please enter a number");

for (let i=number; i>0; i--) {

if (i <= 50) {
	break;
}

if (i % 10 === 0) {
	console.log("Skip and continue to the next iteration.");
	continue;
}	
if (i % 5 === 0) {
    console.log(i);
}	

}



/*	
PART 2:

	- Create a varaible that will contain the string "supercalifragilisticexpialidocious"

	- Create another varaible that will store the consonants from the string.

	- Create a for loop that will iterate through the individual letters of the string based on it's length.

	- Create an if statement that will check if the individual letters of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

	- Create an else statement that will add the consonants to the second variable.


*/

	// CODE HERE:

const myString = "supercalifragilisticexpialidocious";
let myStringConsonants = "";

for (let j=0; j<myString.length; j++) {

if (myString[j].toLowerCase() == "a" ||
	myString[j].toLowerCase() == "e" ||
	myString[j].toLowerCase() == "i" ||
	myString[j].toLowerCase() == "o" ||
	myString[j].toLowerCase() == "u" ) {
	continue;
} else {
	myStringConsonants += myString[j];
}

}

console.log(myStringConsonants)




















